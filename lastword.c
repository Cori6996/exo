#include <unistd.h>

void ft_putchar(char c)
{
	write(1, &c, 1);
}

int ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
		i++;
	return (i);
}

int getstart(char *str)
{
	int len;

	len = ft_strlen(str);
	while (len > 0)
	{
		if (str[len] != ' ' && str[len] != '\0' && str[len - 1] == ' ')
			return (len);
		len--;
	}
	return (len);
}

void lastword(char *str)
{
	int start;

	start = getstart(str);
	while (str[start] != ' ' && str[start] != '\0')
		ft_putchar(str[start++]);
}

int main(int argc, char **argv)
{
	if (argc == 2)
		lastword(argv[1]);
	ft_putchar('\n');
	return (0);
}