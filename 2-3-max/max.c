/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   max.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdupont <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/24 12:28:30 by tdupont           #+#    #+#             */
/*   Updated: 2015/07/24 13:29:53 by tdupont          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		max(int *tab, unsigned int length)
{
	unsigned int i;
	int max;

	i = 0;
	max = tab[i];
	while (i < length)
	{
		if (tab[i] >= max)
			max = tab[i];
		i++;
	}
	return (max);
}

int main(void)
{
	int tab[10];
	tab = tab[ 10, 8, 9, 2 ,6];
	printf("%d", max(tab, 8));
	return(0);
}
