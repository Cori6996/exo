/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alpha_mirror.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdupont <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/24 12:07:04 by tdupont           #+#    #+#             */
/*   Updated: 2015/07/24 12:27:15 by tdupont          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void ft_putchar(char c)
{
	write(1, &c, 1);
}

char	ft_char_rot42(char c)
{
	if (c >= 'A' && c <= 'M')
		return ('Z' - (c - 'A'));
	if (c >= 'a' && c <= 'm')
		return ('z' - (c - 'a'));
	if (c >= 'N' && c <= 'Z')
		return ('A' + ('Z' - c));
	if (c >= 'n' && c <= 'z')
		return ('a' + ('z' - c));
	return (c);
}

void ft_putstr(char *str)
{
	int i;

	i = 0;
	if (!str)
		return;
	while (str[i] != '\0')
		ft_putchar(ft_char_rot42(str[i++]));
}

int main(int argc, char **argv)
{
	if (argc == 2)	
		ft_putstr(argv[1]);
	ft_putchar('\n');
	return(0);
}
