#include <unistd.h>

void ft_putchar(char c)
{
	write(1, &c, 1);
}

void print_bits(unsigned char octet) {

	int i;

	i = 128;
	while (i > 0)
	{
		if (octet & i)
			ft_putchar('1');
		else
			ft_putchar('0');
		i = i / 2;
	}
}

int main(void)
{
	print_bits(1);
	ft_putchar('\n');
	print_bits(2);
	ft_putchar('\n');
	print_bits(4);
	ft_putchar('\n');
	print_bits(8);

	ft_putchar('\n');
	print_bits(0);
	ft_putchar('\n');
	print_bits(3);
	ft_putchar('\n');
	print_bits(6);
	ft_putchar('\n');
	print_bits(10);
	return (0);
}

